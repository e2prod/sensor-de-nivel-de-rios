//Biblioteca para tarjeta SD
#include <SD.h>
#include <avr/wdt.h>
//Biblioteca para sensor ultrasonico HC-SR04
#include <NewPing.h>
#include <avr/sleep.h>
//Biblioteca para timer
#include <Wire.h>
#include <RTClib.h>
// Date and time functions using a DS1307 RTC connected via I2C and Wire lib
// Leonardo	2 (SDA), 3 (SCL)

// sensor temperatura
#include <DHT.h>
// definimos el pin digital
#define DHTPIN 2
//Depende del tipo de sensor
#define DHTTYPE DHT11
//Inicializar el sensor
DHT dht (DHTPIN, DHTTYPE);



const int chipSelect = 4; //Chip select para la SD


//Pines asignados para el ultrasonido
#define TRIGGER_PIN  9       // Disparo que habilita el sensado     
#define ECHO_PIN     8       // Señal de salida del sensor que contiene la informacion sobre la distancia detectada
#define MAX_DISTANCE 400     // Maxima distancia que puede medir el sensor
#define ITERACIONES 5        // Cantidad de iteraciones para el promediado de datos

unsigned long distancia;
unsigned long tiempo,last_meas;

NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE);

RTC_DS1307 rtc;

void setup()
{
 
  //Inicializa la placa del timer. Ya esta puesta en hora
  Wire.begin();
  rtc.begin();
  //inicializamos sensor de temperatura
  dht.begin();
  //Inicializa la tarjeta SD
  pinMode(10, OUTPUT); //El pin de chip select por omision tiene que estar como salida aunque no sea el que se use
  
  //Veo si la tarjeta esta presente y puede ser inicializada:
  if (!SD.begin(chipSelect)) {
    //si no esta, no hace nada mas
    return;
  }

  //La siguiente linea pone en hora el RTC al dia y la fecha en la que se compila este codigo
  //rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
   
  //Veo si el RTC esta corriendo 
  if (! rtc.isrunning()) {
    return;
  }
     pinMode(13,OUTPUT);
     digitalWrite(13,HIGH); // heartbeat
}

void loop()
{
  //Cadena para guardar la medicion
  String stringNivel = ""; 
  String stringHumedad = ""; 
  String stringTemperatura = "";
  
  //Cadenas para armar la fecha y hora
  String stringFecha = ""; 
  String stringAnio = "";
  String stringMes = ""; 
  String stringDia = "";
  String stringHora = "";
  String stringMinuto = ""; 

  //Cadena "fecha, medicion". Es la que se guardara en la SD
  String stringDatos ="";



//////////////////////////////////////////////////////////////////////////////
//ultrasonico//
  int tiempo;
  int distancia; 
  
   //Para calcular los milisegundos para pasarle a la funcion delay
  
  // Se realiza un promedio de los valores registrados, descartando los valores fuera de rango.
  tiempo = sonar.ping_median(ITERACIONES);
  // Se convierte el valor de tiempo a cm
  distancia = sonar.convert_cm(tiempo);
  //Guarda la medicion en la cadena
  stringNivel=String(distancia);
/////////////////////////////////////////////////////////////////////////////////
//temperatura por balta
// leemos la humedad relativa
  float humedad = dht.readHumidity();
// Leemos la temperatura en grados centigrados(por defecto)
  float temperaturaCelsius = dht.readTemperature();
stringHumedad=String(humedad);
stringTemperatura=String(temperaturaCelsius);

////////////////////////////////
  //Levanta la fecha
  DateTime now = rtc.now();
  //Arma las cadenenas para la fecha
  stringAnio=String(now.year(), DEC);
  stringMes=String(now.month(), DEC);
  stringDia=String(now.day(), DEC);
  stringHora=String(now.hour(), DEC);
  stringMinuto=String(now.minute(), DEC);
  stringFecha= stringAnio + "-" + stringMes + "-" + stringDia + "-" + stringHora + "-" + stringMinuto;

  //Arma la cadena a guardar
  stringDatos=stringFecha + ",Nivel: " + stringNivel+ ", Humedad: " + stringHumedad+ ", Temperatura:" + stringTemperatura;


  
  //Abre el archivo. Se puede abrir un solo archivo por vez
  File dataFile = SD.open("datalog.txt", FILE_WRITE);

  //Si el archivo esta disponible, escribe en el:
  if (dataFile) {
    dataFile.println(stringDatos);
    dataFile.close();
  }  
  
  
  digitalWrite(13,LOW);
  wdt_enable(WDTO_8S); // habilita el WDT para despertar em micro cada 8segundos. 
  // hay que setearlo cada vez, por que WDIE se borra cuando salta la interrupción y vuelve al reset
  // por default cuando desborda.
  set_sleep_mode(SLEEP_MODE_PWR_DOWN); // SLEEP_MODE_PWR_DOWN solo para sdespertar con interrupcion externa o WDT
  sleep_mode(); //Utilizar Wakeup con el Watchdog cada 8 segundos. 
  digitalWrite(13,HIGH); // heartbeat
  delay(50); // garantizo un tiempo minimo de parpadeo.
  
}

ISR(WDT_vect){//Rutina que se ejecuta por desborde del WDT
WDTCSR = (1<<WDIE) | (0<<WDE) | (1<<WDP3) | (1<<WDP0);  // 8s / interrupt, no system reset
// hay que setearlo cada vez que salta la int, por que WDIE se borra cuando salta la interrupción y vuelve al reset
}